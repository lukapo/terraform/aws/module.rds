variable "db_name" {
  description = "Name of the database"
  type        = string
}

variable "vpc_id" {
  description = "ID of the VPC to deploy database into"
  type        = string
}

variable "subnet_list" {
  description = "List of subnet IDs database instances should deploy into"
  type        = list(string)
}

variable "engine" {
  description = "Database engine (aurora-postgres, aurora-mysql)"
  type        = string
}

variable "engine_version" {
  description = "Database engine version (eg 5.7.12)"
  type        = string
}

variable "replica_count" {
  description = "Number of read replicas to run"
  type        = number
}

variable "sg_list" {
  description = "List of security groups to apply to the cluster"
  type        = list(string)
}

variable "sg_count" {
  description = "Explicite count of security groups - has to match the number of SGs in sg_list variable"
  type        = number
  default     = 1
}

variable "instance_type" {
  description = "Instance type to be used for the database instances"
  type        = string
}

variable "storage_encrypted" {
  description = "Should be true to encrypt the DB disks"
  type        = bool
  default     = true
}

variable "apply_immediately" {
  description = "Should be true to apply any changes to DB configuration immediatelly, rather than wait for the maintenance window"
  type        = bool
  default     = true
}

variable "monitoring_interval" {
  description = "The interval in seconds determining how often Enhances Monitoring metrics are to be collected"
  type        = number
  default     = 10
}

variable "deletion_protection" {
  description = "Should be false to run the DB with deletion protection off, making sure delete attempts (including terraform) dont result in failure"
  type        = bool
  default     = false
}

variable "parameter_group_name" {
  description = "Name of the parameter group"
  type        = string
}

variable "cluster_parameter_group_name" {
  description = "Name of the cluster parameter group"
  type        = string
}

variable "cw_logs_export_list" {
  description = "List of log types to export to cloudwatch (audit/error/general/slowquery)"
  type        = list(string)
  default     = ["postgresql"]
}

variable "tags" {
  description = "Map containing tags to use for the instance"
  type        = map(string)
}

variable "password" {
  description = "Database password"
  type        = string
}

variable "publicly_accessible" {
  description = "Whether the DB should have a public IP address"
  type        = bool
  default     = false
}