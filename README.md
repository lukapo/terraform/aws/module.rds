# RDS module

## Input Variables
* `apply_immediately` (default `true`): Should be true to apply any changes to DB configuration immediatelly, rather than wait for the maintenance window
* `cluster_parameter_group_name` (required): Name of the cluster parameter group
* `cw_logs_export_list` (default `["postgresql"]`): List of log types to export to cloudwatch (audit/error/general/slowquery)
* `db_name` (required): Name of the database
* `deletion_protection` (required): Should be false to run the DB with deletion protection off, making sure delete attempts (including terraform) dont result in failure
* `engine` (required): Database engine (aurora-postgres, aurora-mysql)
* `engine_version` (required): Database engine version (eg 5.7.12)
* `instance_type` (required): Instance type to be used for the database instances
* `monitoring_interval` (default `10`): The interval in seconds determining how often Enhances Monitoring metrics are to be collected
* `parameter_group_name` (required): Name of the parameter group
* `password` (required): Database password
* `publicly_accessible` (required): Whether the DB should have a public IP address
* `replica_count` (required): Number of read replicas to run
* `sg_count` (default `1`): Explicite count of security groups - has to match the number of SGs in sg_list variable
* `sg_list` (required): List of security groups to apply to the cluster
* `storage_encrypted` (default `true`): Should be true to encrypt the DB disks
* `subnet_list` (required): List of subnet IDs database instances should deploy into
* `tags` (required): Map containing tags to use for the instance
* `vpc_id` (required): ID of the VPC to deploy database into

## Output Values
* `db_host`
* `db_password`

## Child Modules
* `rds` from `terraform-aws-modules/rds-aurora/aws` (`2.2.0`)
