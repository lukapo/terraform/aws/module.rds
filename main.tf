module "rds" {
  source  = "terraform-aws-modules/rds-aurora/aws"
  version = "2.2.0"

  name = var.db_name

  vpc_id  = var.vpc_id
  subnets = var.subnet_list

  engine         = var.engine
  engine_version = var.engine_version

  replica_count          = var.replica_count
  vpc_security_group_ids = var.sg_list
  instance_type          = var.instance_type
  storage_encrypted      = var.storage_encrypted
  apply_immediately      = var.apply_immediately
  monitoring_interval    = var.monitoring_interval
  password               = var.password
  deletion_protection    = var.deletion_protection
  publicly_accessible    = var.publicly_accessible

  db_parameter_group_name         = var.parameter_group_name
  db_cluster_parameter_group_name = var.cluster_parameter_group_name
  enabled_cloudwatch_logs_exports = var.cw_logs_export_list

  tags = var.tags
}

