output "db_password" {
  value = var.password
}

output "db_host" {
  value = module.rds.this_rds_cluster_endpoint
}