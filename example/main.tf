provider "aws" {
  region = "eu-central-1"
}

data "aws_vpc" "main" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.main.id
}

locals {
  project           = "test"
  environment       = "dev"
  env_project       = "${local.environment}-${local.project}"
  postgres_password = "test-password"
}

module "rds" {
  source = "../"

  db_name = "rds-${local.env_project}"

  vpc_id      = data.aws_vpc.main.id
  subnet_list = data.aws_subnet_ids.all.ids

  sg_list = [aws_security_group.allow_postgres.id]

  parameter_group_name         = "default.aurora-postgresql10"
  cluster_parameter_group_name = "default.aurora-postgresql10"

  engine         = "aurora-postgresql"
  engine_version = "10.7"
  password       = local.postgres_password

  publicly_accessible = true

  replica_count = 2

  instance_type = "db.t3.medium"

  tags = {
    Project     = local.project
    Environment = local.environment
  }
}

resource "aws_security_group" "allow_postgres" {
  name        = "security_group-${local.env_project}-allow_all_postgres"
  description = "Allow postgres inbound traffic"

  vpc_id = data.aws_vpc.main.id

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}
